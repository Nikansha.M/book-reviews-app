## PLANNING - SETTING UP PROJECT & APP(s)

[X] create a virtual environment:   python -m venv .venv
[X] activate the virtual environment:  .\.venv\Scripts\Activate.ps1
[X] install django package:  pip3 install django
[X] create the django project:  django-admin startproject <<project-name>> .  --> book_reviews
    (don't forget the dot!!!)
[X] add a django app to the project:  python manage.py startapp <<app-name>> --> reviews
[X] start django development server:  python manage.py runserver
[X] make migrations:  python manage.py makemigrations
[X] migrate:  python manage.py migrate


## CONFIGURE APP TO PROJECT in settings.py 

[X] In settings.py, add the  <<app-name>>  to the  <<project-name>>  in 
    INSTALLED APPS: [
        reviews.apps.ReviewsConfig,
    ]


## CREATE THE DATA MODEL in models.py

[X] In  <<app-name>>/models.py  add/change the Django model class:
    class <Review>(models.Model)
    -   this will include all of the data we want to see for each 
        book review on the web page and lets us interact with data in the database
[X] Model classes like to talk to the database, update database by making and running migrations


## CREATE DJANGO TEMPLATES

[X] Create a folder named "templates" under  <<app-name>>
    [X] Right click "templates" directory and create a folder named "<reviews>" 
        [X] Under "<reviews/templates/reviews>" create a file named main.html


## CREATE VIEW FUNCTION TO LIST STUFF ON WEB PAGE in views.py

[X] In <<app-name>>/views.py create a function that will gather data and send the HTML back to the browser
    [X] from <reviews.models> import <Review>


## CREATE urls.py FOR EACH DJANGO APP

[X] In urls.py file, create root path under urlpatterns for  <<app-name>> 
    -   Ex: path("", list_reviews, name="reviews_list"),
        -   <reviews>  Django app will handle HTTP requests from browsers


## CREATE URL MAPPING FOR VIEW FUNCTION

[X] In  <<app-name>>/urls.py  import view function that we defined in  <<app-name>>/views.py  
    -   Ex: from reviews.views import list_reviews
    -   Python files are modules: reviews/views.py file is the reviews.views module in the Django application
[X] In the  <<project-name>>/urls.py  file, import include function to add urlpatterns from  <<app-name>>/urls.py
    [X] Create a redirect for the root path to the urlpatterns from  <<app-name>>/urls.py
        -   Ex: path("reviews/", include("reviews.urls"))


## ADD CSS FILES

[X] Right click  <<app=name>>  , create a new folder named "static"
[X] Right click "static" folder, create new file named "book-review.css"
[X] In main.html on the first line, type:  {% load static %}
    -   this loads all of the function from the static Django template library
[X] In main.html change link tag to Django template tag with the name static:  <link rel="stylesheet" href="{% static "book-review.css" %}">
    -   this is a function named static in the statuc module


## CHANGING TEMPLATE TO USE DATA - USING LOOPS

[X]  Create a for loop in main.html using the below to loop over content
    {% for review in reviews %} 
    ...<div> content goes here </div>...
    {% endfor %}