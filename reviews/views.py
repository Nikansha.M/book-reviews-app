from django.shortcuts import render

# Create your views here.


from django.shortcuts import render, redirect
from reviews.models import Review
from reviews.forms import ReviewForm


# Create your views here.

def list_reviews(request):
    # get all of the reviews from the database with Review.objects.all()
    reviews = Review.objects.all()
    
    # get all of the reviews and store them in a dictionary named context
    context = {
        "reviews": reviews,
    }
    
    # render HTML using the data from the context variable
    # once HTML is generated it is sent back to the browser
    return render(request, "reviews/main.html", context)


def create_review(request):
    form = ReviewForm()
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("reviews_list")
    context = {
        "form": form,
    }
    return render(request, "reviews/create.html", context)


def review_detail(request, id):
    # new function that gets the id of the review that we want to show and shows it
    review = Review.objects.get(id=id)
    context = {
        "review": review,
    }
    return render(request, "reviews/detail.html", context)