from django.urls import path
# from view.py import the functions we created
from reviews.views import list_reviews, create_review, review_detail

urlpatterns = [
    # list_reviews function
    path("", list_reviews, name="reviews_list"),
    
    # create_review function
    path("new/", create_review, name="create_review"),
    
    # imported the review detail function
    # mapped it to the <int:id> path - creates the id 
    # value from what's in the URL path
    path("<int:id>/", review_detail, name="review_detail"),
]